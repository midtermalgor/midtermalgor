package com.satit.midtermal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

/**
 *
 * @author satit
 */
// A-1.8
public class MidtermAl {
    static void reverseArr(ArrayList<String> array) {
        long StartTime = System.nanoTime();
        int s_index = 0;
        int e_index = array.size() - 1;
        while (s_index < e_index) {
            String temp = array.get(s_index);
            array.set(s_index, array.get(e_index));
            array.set(e_index, temp);
            s_index += 1;
            e_index -= 1;

        }
        long endTime = System.nanoTime();
        long totalTime = endTime - StartTime;
        System.out.println("Time: " + totalTime + " nanoseconds");

    }

    static ArrayList<String> readInputandInserToList(String filetxt) {
        ArrayList<String> list = new ArrayList<>();
        try {
            Scanner numFile = new Scanner(new File(filetxt));
            while (numFile.hasNextLine()) {
                String line = numFile.nextLine();
                Scanner sc = new Scanner(line);
                sc.useDelimiter(" ");
                while (sc.hasNext()) {
                    list.add(sc.next());
                }
                sc.close();
            }
            numFile.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error");
        }
        return list;

    }

    public static void main(String[] args) {
        // code run program cmd is java MidtermAl.java input1.txt
        String inputFiletxt = args[0];

        ArrayList<String> list = readInputandInserToList(inputFiletxt);
        reverseArr(list);
        System.out.println(list);
    }
}
